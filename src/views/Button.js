import React from 'react';

// common component for the button
const Button = ({ name, btnClass, onClickBtn }) => {
  return(
    <button className={`btn ${btnClass}`} onClick={onClickBtn}>{name}</button>
  )
}

export default Button