import React from 'react'
import Snackbar from '@material-ui/core/Snackbar'

//display alert message
const AlertMessage = () => {
  return (
    <Snackbar
      anchorOrigin={{
        vertical: 'top',
        horizontal: 'center',
      }}
      open={true}
      autoHideDuration={1000}
      message="You have done maximum bookings for this week"
    />
  )
}

export default AlertMessage
