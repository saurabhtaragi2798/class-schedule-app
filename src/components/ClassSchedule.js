import Button from '../views/Button'
import React, { useEffect, useState } from 'react'
import { scheduleClassMockData } from '../constants'
import AlertMeesage from '../views/AlertMessage'
import ShoppingCartIcon from '@material-ui/icons/ShoppingCart'
import { Link } from 'react-router-dom'
import Badge from '@material-ui/core/Badge'

const ClassSchedule = () => {
  const [classScheduleData, setclassScheduleData] = useState([]);
  let bookedClasses = JSON.parse(localStorage.getItem('bookedClasses'));


  //not using redux in application so using it on every component
  const [isDisplayAlert, setIsDisplayAlert] = useState(false);
  const [timeLeft, setTimeLeft] = useState(0);

  useEffect(() => {
    let data = [...scheduleClassMockData]

    //generate availability according to requirement
    data?.forEach(item => {
      const randomNumber = Math.floor(Math.random() * 15)
      item.availability = randomNumber
      item.isBooked = false
    })

    //used local storage for storing data
    localStorage.setItem('bookedClasses', JSON.stringify([]))

    setclassScheduleData(data);

    //generate no for time left between 30 to 60
     setTimeLeft(Math.floor(Math.random() * 31) + 30);
    
  }, []);

  useEffect(() => {
    timeLeft > 0 && setTimeout(() => setTimeLeft(timeLeft - 1), 1000);
  })

  const handleClick = (e, item) => {
    let index = classScheduleData.findIndex(
      scheduleClass => scheduleClass?.id === item?.id
    )

    if (bookedClasses.length < 3 && !item.isBooked && item?.availability > 0) {
      item.availability = item?.availability - 1;
      bookedClasses.push(item)
      localStorage.setItem('bookedClasses', JSON.stringify(bookedClasses))

      if (index !== -1) {
        classScheduleData[index].isBooked = true
        classScheduleData[index].availability =
          classScheduleData[index].availability > 0
            ? item.availability
            : 0
        setclassScheduleData([...classScheduleData]);
      }
    } else if(item?.availability > 0 &&  !classScheduleData[index].isBooked) {
      setIsDisplayAlert(true)
    }
  }

  return (
    <div className='container'>
      {isDisplayAlert && <AlertMeesage />}
      <div className="mt-3 cart-icon">
        <Link to='/cart'>
          <Badge badgeContent={bookedClasses?.length} color='primary'>
              <ShoppingCartIcon />
          </Badge>
        </Link>
      </div>
      <div className='row mt-3'>
        <p className='fw-bolder'>Time Left: {timeLeft} seconds</p>
        <p className='fs-4 claim-text fw-bolder'>Claim Your Free Trial Class</p>
        <p className='fs-4 schedule-text fw-bolder'>Class Schedule</p>

        <table className='table table-responsive table-bordered'>
          <thead>
            <tr>
              <th>Subject</th>
              <th>Date</th>
              <th>Time</th>
              <th>Availability</th>
              <th></th>
            </tr>
          </thead>
          <tbody>
            {classScheduleData?.map((item, i) => {
              return (
                <tr key={`${item?.subject}_${i}`}>
                  <td>{item?.subject}</td>
                  <td>{item?.date}</td>
                  <td>{item?.time}</td>
                  <td>{item?.availability} seats available</td>
                  <td>
                    <Button
                      name={`${
                        item?.availability > 0
                          ? item?.isBooked
                            ? 'Booked'
                            : 'Book Now'
                          : 'Full'
                      }`}
                      btnClass={`${
                        item?.availability === 0 ? 'full-btn' : 'book-now-btn'
                      }`}
                      onClickBtn={e => handleClick(e, item)}
                    />
                  </td>
                </tr>
              )
            })}
            <tr></tr>
          </tbody>
        </table>
      </div>
    </div>
  )
}

export default ClassSchedule
