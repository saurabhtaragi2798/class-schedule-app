import React, { useState, useEffect } from 'react'
import Button from '../views/Button'

//it will dispaly cart list
const Cart = () => {
  const [bookedClasses, setBookedClasses] = useState([])

  useEffect(() => {
    //this data get from API but here used local storage
    let respData = JSON.parse(localStorage.getItem('bookedClasses'))
    setBookedClasses(respData)
  }, [])

  const handleClick = item => {
    //remove element from the localstorage and state
    const index = bookedClasses.findIndex(
      bookedClass => bookedClass.id === item.id
    )

    if (index !== -1) {
      bookedClasses.splice(index, 1)
    }

    localStorage.setItem('bookedClasses', JSON.stringify([...bookedClasses]))
    setBookedClasses([...bookedClasses])
  }

  return (
    <div className='container mt-5'>
      {bookedClasses?.length === 0 && <p className="text-center fw-bolder mt-5">No Booked Class Found</p>}
      {bookedClasses?.length > 0 && <table className='table table-responsive table-bordered'>
        <thead>
          <tr>
            <th>Subject</th>
            <th>Date</th>
            <th>Time</th>
            <th>Availability</th>
            <th></th>
          </tr>
        </thead>
        <tbody>
          {bookedClasses?.map((item, i) => {
            return (
              <tr key={`${item?.subject}_${i}`}>
                <td>{item?.subject}</td>
                <td>{item?.date}</td>
                <td>{item?.time}</td>
                <td>{item?.availability} seats available</td>
                <td>
                  <Button
                    name='Cancel'
                    btnClass='cancel-btn'
                    onClickBtn={() => handleClick(item)}
                  />
                </td>
              </tr>
            )
          })}
          <tr></tr>
        </tbody>
      </table>}
    </div>
  )
}

export default Cart
