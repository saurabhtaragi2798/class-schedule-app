import React from 'react'
import { Switch, Route } from 'react-router-dom'
import ClassSchedule from '../components/ClassSchedule'
import Cart from '../components/Cart'

const MyRoutes = () => {
  return (
    <Switch>
      <Route exact path='/' component={ClassSchedule} />
      <Route exact path='/cart' component={Cart} />
    </Switch>
  )
}

export default MyRoutes
