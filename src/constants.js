export const scheduleClassMockData = [
  {
    id: 1,
    subject: 'Python',
    date: 'Mon May 03 2021',
    time: '04:00PM - 05:00PM',
  },
  {
    id: 2,
    subject: 'Python',
    date: 'Mon May 10 2021',
    time: '04:00PM - 05:00PM',
  },
  {
    id: 3,
    subject: 'Python',
    date: 'Mon May 17 2021',
    time: '04:00PM - 05:00PM',
  },
  {
    id: 4,
    subject: 'Python',
    date: 'Mon May 24 2021',
    time: '04:00PM - 05:00PM',
  },
  {
    id: 5,
    subject: 'Python',
    date: 'Mon May 31 2021',
    time: '04:00PM - 05:00PM',
  },
  {
    id: 6,
    subject: 'Python',
    date: 'Mon Jun 07 2021',
    time: '04:00PM - 05:00PM',
  },
  {
    id: 7,
    subject: 'Python',
    date: 'Mon Jun 14 2021',
    time: '04:00PM - 05:00PM',
  },
  {
    id: 8,
    subject: 'Python',
    date: 'Mon Jun 21 2021',
    time: '04:00PM - 05:00PM',
  },
]

